import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MultiplicationTest {

    private static final Logger logger = LogManager.getLogger(MultiplicationTest.class);
    private static Multiplication multiplication;

    @BeforeClass
    public static void init() {
        multiplication = new Multiplication();
    }

    @Test
    public void testMultiplicationPositiveValues() {
        logger.info("testMultiplicationPositiveValues started");
        Assert.assertEquals(400, multiplication.operation(20, 20));
        logger.info("testMultiplicationPositiveValues finished");
    }

    @Test
    public void testMultiplicationNegativeValues() {
        logger.info("testMultiplicationNegativeValues started");
        Assert.assertEquals(2000, multiplication.operation(-50, -40));
        logger.info("testMultiplicationNegativeValues finished");
        logger.warn("Tested log");
    }

    @Test
    public void testMultiplicationOneNegativeOnePositiveValues() {
        logger.info("testMultiplicationOneNegativeOnePositiveValues started");
        Assert.assertEquals(-1200, multiplication.operation(-40, 30));
        logger.info("testMultiplicationOneNegativeOnePositiveValues finished");
    }

    @Test
    public void testMultiplicationOnePositiveOneNegativeValues() {
        logger.info("testMultiplicationOnePositiveOneNegativeValues started");
        Assert.assertEquals(-1400, multiplication.operation(70, -20));
        logger.info("testMultiplicationOnePositiveOneNegativeValues finished");
        logger.debug("Tested log");
    }
}