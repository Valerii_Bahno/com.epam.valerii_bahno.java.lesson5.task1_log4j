import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SubtractionTest {

    private static final Logger logger = LogManager.getLogger(SubtractionTest.class);
    private static Subtraction subtraction;

    @BeforeClass
    public static void init() {
        subtraction = new Subtraction();
    }

    @Test
    public void testSubtractionPositiveValues() {
        logger.info("testSubtractionPositiveValues started");
        Assert.assertEquals(80, subtraction.operation(100, 20));
        logger.info("testSubtractionPositiveValues finished");
    }

    @Test
    public void testSubtractionNegativeValues() {
        logger.info("testSubtractionNegativeValues started");
        Assert.assertEquals(-30, subtraction.operation(-80, -50));
        logger.info("testSubtractionNegativeValues finished");
        logger.warn("Tested log");
    }

    @Test
    public void testSubtractionOneNegativeOnePositiveValues() {
        logger.info("testSubtractionOneNegativeOnePositiveValues started");
        Assert.assertEquals(-90, subtraction.operation(-70, 20));
        logger.info("testSubtractionOneNegativeOnePositiveValues finished");
        logger.debug("Tested log");
    }

    @Test
    public void testSubtractionOnePositiveOneNegativeValues() {
        logger.info("testSubtractionOnePositiveOneNegativeValues started");
        Assert.assertEquals(130, subtraction.operation(90, -40));
        logger.info("testSubtractionOnePositiveOneNegativeValues finished");
    }
}