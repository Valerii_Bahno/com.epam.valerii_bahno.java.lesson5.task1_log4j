import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AdditionTest {

    private static final Logger logger = LogManager.getLogger(AdditionTest.class);
    private static Addition addition;

    @BeforeClass
    public static void init() {
        addition = new Addition();
    }

    @Test
    public void testAdditionPositiveValues() {
        logger.info("testAdditionPositiveValues started");
        Assert.assertEquals(20, addition.operation(10, 10));
        logger.info("testAdditionPositiveValues finished");
    }

    @Test
    public void testAdditionNegativeValues() {
        logger.info("testAdditionNegativeValues started");
        Assert.assertEquals(-40, addition.operation(-10, -30));
        logger.info("testAdditionNegativeValues finished");
        logger.debug("Tested log");
    }

    @Test
    public void testAdditionOneNegativeOnePositiveValues() {
        logger.info("testAdditionOneNegativeOnePositiveValues started");
        Assert.assertEquals(-10, addition.operation(-40, 30));
        logger.info("testAdditionOneNegativeOnePositiveValues finished");
        logger.warn("Tested log");
    }

    @Test
    public void testAdditionOnePositiveOneNegativeValues() {
        logger.info("testAdditionOnePositiveOneNegativeValues started");
        Assert.assertEquals(20, addition.operation(50, -30));
        logger.info("testAdditionOnePositiveOneNegativeValues finished");
    }
}