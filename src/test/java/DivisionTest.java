import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DivisionTest {

    private static final Logger logger = LogManager.getLogger(DivisionTest.class);
    private static Division division;

    @BeforeClass
    public static void init() {
        division = new Division();
    }

    @Test
    public void testDivisionPositiveValues() {
        logger.info("testDivisionPositiveValues started");
        Assert.assertEquals(4, division.operation(80, 20));
        logger.info("testDivisionPositiveValues finished");
        logger.debug("Tested log");
    }

    @Test(expected = ArithmeticException.class)
    public void testThrowRuntimeExceptionWhenZeroDivision() {
        logger.info("testThrowRuntimeExceptionWhenZeroDivision started");
        logger.warn("Argument b = 0");
        logger.error("Division by zero is prohibited!");
        Assert.assertEquals(ArithmeticException.class, division.operation(100, 0));
        logger.info("testThrowRuntimeExceptionWhenZeroDivision finished");
        logger.debug("Tested log");
    }

    @Test
    public void testDivisionNegativeValues() {
        logger.info("testDivisionNegativeValues started");
        Assert.assertEquals(2, division.operation(-50, -25));
        logger.info("testDivisionNegativeValues finished");
    }

    @Test
    public void testDivisionOneNegativeOnePositiveValues() {
        logger.info("testDivisionOneNegativeOnePositiveValues started");
        Assert.assertEquals(-6, division.operation(-60, 10));
        logger.info("testDivisionOneNegativeOnePositiveValues finished");
        logger.error("Tested log - error");
    }

    @Test
    public void testDivisionOnePositiveOneNegativeValues() {
        logger.info("testDivisionOnePositiveOneNegativeValues started");
        Assert.assertEquals(-5, division.operation(100, -20));
        logger.info("testDivisionOnePositiveOneNegativeValues finished");
        logger.warn("Tested log");
    }
}