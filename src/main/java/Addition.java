public class Addition extends Operation {

    @Override
    int operation(int a, int b) {
        return (a + b);
    }
}
