public class Division extends Operation{

    @Override
    int operation(int a, int b) {

        if (b == 0) {
            throw new ArithmeticException("Incorrect value of B - division by 0 is impossible!");
        }
        return (a / b);
    }
}
