abstract class Operation {

    abstract int operation(int a, int b);
}