import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        /* Enter integer values A and B from the keyboard */
        System.out.println("Enter integer value A:");
        int a = in.nextInt();
        System.out.println("Enter integer value B (B > 0):");
        int b = in.nextInt();
        /* Enter the operation from the keyboard (add, subtract, multiplic, div) */
        System.out.println("Enter the operation: " + "add / subtract / multiplic / div");
        String c = in.next();

        operation(a, b, c);
    }

    public static void operation(int a, int b, String c) {
        /* Operations with values A and B */
        switch (c) {
            case "add":
                System.out.println("Sum of values = " + new Addition().operation(a, b));
                break;
            case "subtract":
                System.out.println("Difference of values = " + new Subtraction().operation(a, b));
                break;
            case "multiplic":
                System.out.println("Multiplying values = " + new Multiplication().operation(a, b));
                break;
            case "div":
                System.out.println("Division of values = " + new Division().operation(a, b));
                break;
            default:
                System.out.println("You entered wrong operation!");
        }
    }
}
